import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/films/:filmId',
    name: 'FilmDetails',
    component: () => import('../views/FilmDetails.vue')
  },
  {
    path: '/about',
    name: 'About',
    props: true,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/modify-film-database/:filmId',
    name: 'ModifyFilmDatabase',
    component: () => import('../views/ModifyFilmDatabase.vue')
  },
  {
    path: '/register',
    name: 'Register',
    props: true,
    component: () => import('../views/Register.vue')
  },

]

const router = new VueRouter({
  routes
})

export default router
