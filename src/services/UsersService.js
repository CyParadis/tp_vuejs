import axios from 'axios'
/*
Route::post('users/login' pour se logger et prend le username et pasword
Route::post('users' pour ajouter un user et prend tout les champs d'un formulaire

Route::put('users/{user}' qui modifie tout sauf le password (et le id évidemment) *les puts prennent le token aussi
Route::put('users/{user}/password' qui modifie juste le password d'un user


Route::get('users/{user}'
Route::get('users/{login}/verify' 
-->*/
const apiClient = axios.create({
  baseURL: 'http://radiant-plains-67953.herokuapp.com/api/',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

apiClient.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    console.error('API Error, ' + error);
    return Promise.reject(error);
  }
);


const USER_RESOURCE_NAME = 'users'

export default {

addUser(newUserToAdd){
    return apiClient.post(USER_RESOURCE_NAME, newUserToAdd);
},

//Retourne un token
logginUser(logginData){
    return apiClient.post(USER_RESOURCE_NAME + '/' + 'login', logginData);
  },

//On doit utiliser le token pour avoir les infos d'un utilisateur pour savoir si il existe déjà
getUserInfos(userId, tokenString){
    let newApiClient = axios.create({
      baseURL: 'http://radiant-plains-67953.herokuapp.com/api/', 
      headers: {
        Authorization: 'Bearer '+tokenString,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
    return newApiClient.get(USER_RESOURCE_NAME + '/' + userId);
  },

//si le nom de login est utilisé !
verifyLoggin(login){
    return apiClient.get(USER_RESOURCE_NAME + '/' + login + '/verify');
  },

modifyUser(id, user, tokenString){
  let newApiClient = axios.create({
    baseURL: 'http://radiant-plains-67953.herokuapp.com/api/', 
    headers: {
      Authorization: 'Bearer '+tokenString,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    }
  })
  return newApiClient.put(USER_RESOURCE_NAME + '/' + id, user);
  },
  
modifyPasswordUser(userId, user, tokenString){
  let newApiClient = axios.create({
    baseURL: 'http://radiant-plains-67953.herokuapp.com/api/', 
    headers: {
      Authorization: 'Bearer '+tokenString,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    }
  })
  return newApiClient.put(USER_RESOURCE_NAME + '/' + userId +'/password', user);
  }
}
