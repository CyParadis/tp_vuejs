import axios from 'axios'

const apiClient = axios.create({
  baseURL: 'http://radiant-plains-67953.herokuapp.com/api/',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

apiClient.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    console.error('API Error, ' + error);
    return Promise.reject(error);
  }
);

const RESOURCE_NAME = 'films';

export default {
  getFilms() {
    return apiClient.get(RESOURCE_NAME);
  },
  getFilmById(id) {
    return apiClient.get(RESOURCE_NAME + '/' + id);
  },
  getActorsByFilm(id) {
    return apiClient.get(RESOURCE_NAME + '/' + id + '/actors');
  },
  getFilmsByPage(noPage) {
    return apiClient.get(RESOURCE_NAME + '?page=' + noPage);
  },
  getFilmByKeyword(keyword, page=1) {
    return apiClient.get(RESOURCE_NAME + '?keyword=' + keyword + '&page=' + page);
  },
  getFilmPossibleRatings(){
    return apiClient.get('ratings')
  },
  getFilmPossibleLanguages(){
    return apiClient.get('languages')
  },
  addFilm(film){
    let newApiClient = axios.create({
      baseURL: 'http://radiant-plains-67953.herokuapp.com/api/', 
      headers: {
        Authorization: 'Bearer '+ sessionStorage.getItem('token'),
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
    return newApiClient.post(RESOURCE_NAME, film);
  },
  updateFilm(oldFilm, updatedFilm){
    let newApiClient = axios.create({
      baseURL: 'http://radiant-plains-67953.herokuapp.com/api/', 
      headers: {
        Authorization: 'Bearer '+ sessionStorage.getItem('token'),
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
    return newApiClient.put(RESOURCE_NAME + '/' + oldFilm.id, updatedFilm);
  },
  deleteFilm(filmId){
    let newApiClient = axios.create({
      baseURL: 'http://radiant-plains-67953.herokuapp.com/api/', 
      headers: {
        Authorization: 'Bearer '+ sessionStorage.getItem('token'),
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
    return newApiClient.delete(RESOURCE_NAME + '/' + filmId);
  },
  addCritic(filmId, critic){
    let newApiClient = axios.create({
      baseURL: 'http://radiant-plains-67953.herokuapp.com/api/', 
      headers: {
        Authorization: 'Bearer '+ sessionStorage.getItem('token'),
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
    return newApiClient.post(RESOURCE_NAME + '/' + filmId + '/critics', critic);
  },
  updateCritic(filmId, criticId, critic){
    let newApiClient = axios.create({
      baseURL: 'http://radiant-plains-67953.herokuapp.com/api/', 
      headers: {
        Authorization: 'Bearer '+ sessionStorage.getItem('token'),
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
    return newApiClient.post(RESOURCE_NAME + '/' + filmId + '/critics', critic);
  },
}
